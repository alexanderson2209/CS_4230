#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <omp.h>
using namespace std;
int main(int argc, char* argv[]) {
  if (argc != 4) {
    cout << "Help: OMP_DP <probsize> <threshold> <numthreads>" << endl;
    exit(EXIT_FAILURE);
  }
  // Grabbing arguments from command line
  int probSize = strtol(argv[1], NULL, 10);
  int threshold = strtol(argv[2], NULL, 10);
  int numThreads = strtol(argv[3], NULL, 10);

  // Initializing arrays a, b, and result.
  double a [probSize];
  double b [probSize];
  double result [probSize];

  // Populate the arrays a and b with random data
  for(int i = 0; i < probSize; i += 1) {
    a[i] = rand() % 16;
    b[i] = rand() % 16;
  }

#pragma omp parallel num_threads(numThreads)
{
#pragma omp for
  for(int i = 0; i < probSize; i += 1)
    {
        // Populate results with results of Dot Product.
        result[i] = a[i] * b[i];
    }
 }
  // Printing results
  cout << endl;
  cout << "A:" << endl;
  for (int i = 0; i < probSize; i++)
      cout << a[i] << ' ';
  cout << endl;

  cout << "B:" << endl;
  for (int i = 0; i < probSize; i++)
        cout << b[i] << ' ';
  cout << endl;

  cout << "Results:" << endl;
  for (int i = 0; i < probSize; i++)
        cout << result[i] << ' ';
  cout << endl;
}
