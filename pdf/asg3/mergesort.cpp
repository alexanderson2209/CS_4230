#include <iostream>
#include <tbb/tbb.h>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

using namespace std;
using namespace tbb;
typedef int T;

// merge sequences [xs,xe) and [ys,ye) to output [zs,(xe-xs)+(ye-ys)
void parallel_merge( const T* xs, const T* xe,  const T* ys, const T* ye, T* zs ) {
    const size_t MERGE_CUT_OFF = 2000;
    if( xe-xs + ye-ys <= MERGE_CUT_OFF ) {
        std::merge(xs,xe,ys,ye,zs);
    } else {
        const T *xm, *ym;
        if( xe-xs < ye-ys  ) {
            ym = ys+(ye-ys)/2;
            xm = std::upper_bound(xs,xe,*ym);
        } else {
            xm = xs+(xe-xs)/2;
            ym = std::lower_bound(ys,ye,*xm);
        }
        T* zm = zs + (xm-xs) + (ym-ys);
        tbb::parallel_invoke( [=]{parallel_merge( xs, xm, ys, ym, zs );},
                              [=]{parallel_merge( xm, xe, ym, ye, zm );} );
    }
}

// sorts [xs,xe).  zs[0:xe-xs) is temporary buffer supplied by caller.
// result is in [xs,xe) if inplace==true, otherwise in zs[0:xe-xs)
void parallel_merge_sort( T* xs, T* xe, T* zs, bool inplace ) {
    const size_t SORT_CUT_OFF = 500;
    if( xe-xs<=SORT_CUT_OFF ) {
        std::stable_sort( xs, xe );
        if( !inplace )
            std::move( xs, xe, zs );
    } else {
       T* xm = xs + (xe-xs)/2;
       T* zm = zs + (xm-xs);
       T* ze = zs + (xe-xs);
       tbb::parallel_invoke( [=]{parallel_merge_sort( xs, xm, zs, !inplace );},
                             [=]{parallel_merge_sort( xm, xe, zm, !inplace );} );
       if( inplace )
           parallel_merge( zs, zm, zm, ze, xs );
       else
           parallel_merge( xs, xm, xm, xe, zs );
   }
}

void call_parallel_merge_sort( T* xs, T* xe ) {
    T* zs = new T[xe-xs];
    parallel_merge_sort( xs, xe, zs, true );
    delete[] zs;
}

//--
#define MAX_ASIZE 1000000000
int ASize;  // array size to be sorted
void Get_args(int argc, char** argv);
void Usage(char* prog_name);
int* A;  // Global array sorted

int main(int argc, char** argv) {
  Get_args(argc, argv);
  A = new int[ASize];
  srand(17);
  // The public method ’init’ used below is
  // documented at https://software.intel.com/en-us/node/506296
  int nthr = 24;
  cout << "Will run upto " << nthr << " threads" << endl;
  // Iterate over all the threads allowed
  for (int p = 1; p <= nthr; ++p) {
    for (int i = 0; i < ASize; ++i) {
      A[i] = rand() % 32768;
    }
    if (ASize < 17)  // Print for small arrays
    {
      for (int i = 0; i < ASize; ++i) {
        cout << "A[" << i << "] = " << A[i] << endl;
      }
    }
    tick_count t0 = tick_count::now();
    // Construct task scheduler with p threads
    // re-initialize task-scheduler to carry p threads
    task_scheduler_init init(p);
    //--
    call_parallel_merge_sort(&A[0], &A[ASize]);  // sort [first,last)
    //--
    tick_count t1 = tick_count::now();
    double t = (t1 - t0).seconds();
    if (ASize < 17) {
      for (int i = 0; i < ASize; ++i) {
        cout << "A[" << i << "] = " << A[i] << endl;
      }
    }
    cout << "ASize = " << ASize << ", nThr = " << p << ", T = " << t << endl;
  }  // for (int p= 1; ...)
  delete[] A;
  return 0;
}

//---
void Get_args(int argc, char** argv) {
  if (argc != 2) Usage(argv[0]);
  ASize = strtol(argv[1], NULL, 10);
  if (ASize <= 0 || ASize > MAX_ASIZE) Usage(argv[0]);
}
void Usage(char* prog_name) {
  fprintf(stderr, "usage: %s ASize:int\n", prog_name);
  exit(0);
}
