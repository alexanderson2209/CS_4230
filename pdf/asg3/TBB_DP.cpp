#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <vector>
#include <algorithm>
#include <timer.h>
#include "tbb/tbb.h"
using namespace std;

// Struct used for TBB parallel for each
struct DpTask {
    int _low;
    int _high;
    double * _a;
    double * _b;
    double * _result;
  DpTask(int low, int high, double a[], double b[], double result[])
    :_low(low),
    _high(high),
    _a(a),
    _b(b),
    _result(result)
  {}
  // Operator to calculate result of DP
  void operator()() {
    for (int i = _low; i < _high; i += 1) {
        _result[i] = _a[i] * _b[i];
    }
  }
};

// Used to invoke DpTask's operator method.
template <typename T> struct invoker {
  void operator()(T& it) const {it();}
};

int main(int argc, char* argv[]) {
  if (argc != 4) {
    cout << "Help: TBB_DP <probsize> <threshold> <numthreads>" << endl;
    exit(EXIT_FAILURE);
  }
  // Grabbing arguments from command line
  int probSize = strtol(argv[1], NULL, 10);
  int threshold = strtol(argv[2], NULL, 10);
  int numThreads = strtol(argv[3], NULL, 10);

  double start, finish, elapsed;
  GET_TIME(start);

  // Initializing arrays a, b, and result.
  double a [probSize];
  double b [probSize];
  double result [probSize];

  // Populate the arrays a and b with random data
  for(int i = 0; i < probSize; i += 1) {
    a[i] = rand() % 16;
    b[i] = rand() % 16;
  }
  // Populate tasks with correct partition of data
  tbb::task_scheduler_init init(numThreads);
  std::vector<DpTask> dpTasks;
  int partitionedSize = probSize / numThreads;
  for (int i = 0; i < probSize; i += partitionedSize) {
    int start = i;
    int end = std::min(start + partitionedSize, probSize);
    dpTasks.push_back(DpTask(start, end, a, b, result));
  }

  // Start for each to solve DP
  tbb::parallel_for_each(dpTasks.begin(),dpTasks.end(),invoker<DpTask>());

  GET_TIME(finish);

  elapsed = finish - start;
  printf("The elapsed time is %e seconds\n", elapsed);

  // Printing results
  cout << endl;
  cout << "A:" << endl;
  for (int i = 0; i < probSize; i++)
      cout << a[i] << ' ';
  cout << endl;

  cout << "B:" << endl;
  for (int i = 0; i < probSize; i++)
        cout << b[i] << ' ';
  cout << endl;

  cout << "Results:" << endl;
  for (int i = 0; i < probSize; i++)
        cout << result[i] << ' ';
  cout << endl;
}
