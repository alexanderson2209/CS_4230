
Grader: Ganesh:
Overall comments:
* Good answers. See comments below. 
Points Awarded: 95

---

Q1: Pretty decent answer. It would be nice to see you dig a little bit
    more into the equations they propose (not to worry, I'll discuss
    them in class).

Q2: Good answer. But more than uniformity, it is the fact that we have
    the single latest value that each line has (i.e. we never get
    incoherent or inconsistent views, with one core thinking the latest
    value is 23 while another thinking it is 2018)

Q3: Very good calculations and convincing answer.

Q4: Nice answer!! You really picked out the real nugget about running
    the program you thought you wrote!

Q5: Good answer. I'll show you the details of these instructions
    in class.

Q6: Good observations on reorderings.

Q7: Good that you dug into C++ constructs and practiced one example
    in detail (that of map)

---

    
