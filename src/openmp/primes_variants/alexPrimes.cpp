#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <omp.h>
#include <vector>
#include "tbb/tbb.h"

using namespace std;
int main(int argc, char* argv[]) {
  if (argc < 3) {
    cout << "Plz provide # threads and prime range" << endl;
    exit(EXIT_FAILURE);
  }
  // Using concurrent vector from tbb to collect all primes
  tbb::concurrent_vector<int> primes;

  int thread_count = strtol(argv[1], NULL, 10);
  int primes_range = strtol(argv[2], NULL, 10);
  cout << "thread_count and primes_range are " << thread_count
       << ", " << primes_range << endl;
#pragma omp parallel num_threads(thread_count)
{
#pragma omp for
  for(int i = 3; i <= primes_range; i += 2)
    { int limit, prime;
      limit = (int) sqrt((float)i) + 1;
      prime = 1; // assume number is prime
      int j = 3;
      while (prime && (j <= limit))
	{
	  if (i%j == 0) prime = 0;
	  j += 2;
	}
      if (prime)
	{
	  // If prime, we push the prime number onto the concurrent vector
	  primes.push_back(i);
	}
    }
 }
 cout << "Printing primes" << endl;
 for (tbb::concurrent_vector<int>::const_iterator i = primes.begin(); i != primes.end(); ++i)
     std::cout << *i << ' ';

 cout << endl;
}
/*
 time primes3.exe 16 40000000
thread_count and primes_range are 16, 40000000
Num primes is:2433653
numP41 is:1216687
numP43 is:1216966
real    0m3.777s
user    0m39.992s
sys     0m0.000s
*/
