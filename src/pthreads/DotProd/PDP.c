#include <stdio.h>
//#include <sys/sysinfo.h>
#include <pthread.h> 
#include <stdlib.h>
#include <math.h>
#include <timer.h>
#define MAX_THREADS 50

const long MAX_EXP = 32;

pthread_t id[MAX_THREADS];
int idNum = 0;

int Exp, Thres;

typedef struct {
  int L;
  int H;
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

pthread_mutex_t lok = PTHREAD_MUTEX_INITIALIZER;

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
   fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
   exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
   if (argc != 3) Usage(argv[0]);
   Exp = strtol(argv[1], NULL, 10);  
   if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1 || Thres > (int) pow(2, Exp)) Usage(argv[0]);
}  

void serdp(RNG *rng) {
  for(int i=rng->L; i<=rng->H; ++i) 
    C[i] = A[i] * B[i];
}

void *pdp(void *myrng)
{
  RNG *rng = (RNG *) myrng;
  printf("-> rng->L and rng->H are %d %d\n", rng->L, rng->H);
  // if size is below threshold, do DP serially
  if ((rng->H - rng->L) <= Thres) {
    serdp(rng);
  }
  else {
    printf("-> rng->L and rng->H are %d %d\n", rng->L, rng->H);
    
    // Setting rngL and RHS to initial rng
    //my_struct* bb = malloc(sizeof(my_struct));
    RNG* rngL = malloc(sizeof(RNG));
    RNG* rngH = malloc(sizeof(RNG));

    *rngL = *rng;
    *rngH = *rng;
    
    // Splitting into 2 RNG. RNGL is lefthand side, RNGH is righthand side
    // rngL gets high changed to low + (high - low)/2. If rng->L was 0 and rng->H was 10, rngL new value is 0 + (10 - 0) /2 = 5
    // rngH gets low value changed to rngL->H + 1. Which is 6.
    // rngL->L = 0, rngL->H = 5
    // rngH->L = 6, rngH->H = 10
    rngL->H = rngL->L + (rngL->H - rngL->L)/2;
    rngH->L = rngL->H+1;

    printf("--> creating thread for range %d %d\n", rngL->L, rngL->H);
    // Locking to prevent multiple threads with same IDs
    pthread_mutex_lock(&lok);
          idNum = idNum + 1;
          pthread_mutex_unlock(&lok);
    pthread_create(&id[idNum], NULL, (void *) pdp, (void *) rngL);

    
    printf("--> creating thread for range %d %d\n", rngH->L, rngH->H);
    // Locking to prevent multiple threads with same IDs
    pthread_mutex_lock(&lok);
          idNum = idNum + 1;
          pthread_mutex_unlock(&lok);
    pthread_create(&id[idNum], NULL, (void *) pdp, (void *) rngH);
  }
  // free rng variable
  free(rng);
  return 0;
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main(int argc, char **argv) {
  // Turn this on on Linux systems
  // On Mac, it does not work
  double start, finish, elapsed;
  GET_TIME(start);
  printf("This system has\
          %d processors configured and\
  	      %d processors available.\n",
          get_nprocs_conf(), get_nprocs());

  Get_args(argc, argv);  
  int Size = (int) pow(2, Exp);
  printf("Will do DP of %d sized arrays\n", Size);

   // Adding random values to arrays A and B. These will be dotted together
  A = (float *) malloc(Size*sizeof(float));
  B = (float *) malloc(Size*sizeof(float));
  printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  // Printing them
  for(int i=0; i<Size; ++i) {
    printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
  }
    // RNG = L and H ints
  RNG *rng;
  rng = (RNG *) malloc(sizeof(RNG));
  rng->L = 0;
  rng->H = Size-1;
  //printf("Serial dot product is %f\n", serdp(rng));

  int sizeCounter = Size;
  int numberOfThreads = 0;

// Getting number of threads used.
  while (sizeCounter != 1 && sizeCounter != 0) {
    sizeCounter = sizeCounter / Thres;
    numberOfThreads = numberOfThreads + sizeCounter;
  }
  
  // initializing answer values in array C to 0
  printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  // Parallel part
  pthread_create(&id[idNum], NULL, (void *) pdp, (void *) rng);

// Join the threads
  for (int i = 0; i < numberOfThreads; i++) {
  printf("pThread: %d\n", i);
   pthread_join(id[i], NULL);
  }

  printf("Final C is\n");
  for(int i=0; i<Size; ++i) {
    printf("%f\n", (double) C[i]);
  }
  GET_TIME(finish);

  elapsed = finish - start;
  printf("The elapsed time is %e seconds\n", elapsed);

  free(A);
  free(B);
  free(C);
  return 0;
}
