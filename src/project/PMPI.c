#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

static unsigned long begin;
int threadRank;

int MPI_Init(int *argc, char***argv){
  //printf("MPI_INIT\n");
  // fprintf(fp, "[{\"t\":0,\"command\":\"create goroutine\",\"name\":\"main\"},\n");
  begin = time(NULL);
  return PMPI_Init(argc,argv);
}

int MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest, int tag,
             MPI_Comm comm) {
               sleep(2);
               unsigned long send_start = time(NULL);
               send_start = (send_start - begin);

               PMPI_Send(buf, count, datatype, dest, tag, comm);
               sleep(1);
               unsigned long send_end = ((time(NULL) - begin) - send_start);
               printf("{\"t\":%u,\"command\":\"send to channel\",\"from\":\"Thread #%d\",\"to\":\"Thread #%d\",\"ch\":\"out\",\"value\":62,\"duration\":%u},\n", (unsigned long)send_start, threadRank, dest, (unsigned long)send_end);
             }

int MPI_Comm_size(MPI_Comm comm,int *size){
  PMPI_Comm_size(comm,size);
}

int MPI_Comm_rank(MPI_Comm comm,int *rank){
  sleep(1);
  unsigned long createTime = time(NULL);
  createTime = (createTime - begin);
  
  PMPI_Comm_rank(comm,rank);
  printf("{\"t\":%u,\"command\":\"create goroutine\",\"name\":\"Thread #%d\",\"parent\":\"main\"},\n", (unsigned long)createTime, *rank);
  threadRank = *rank;
}

int MPI_Get_processor_name(char *name, int *resultlen){
  // printf("MPI-get-processor-name\n");
  return PMPI_Get_processor_name(name,resultlen);
}
int MPI_Finalize(){
  // printf("MPI-Finalize\n");
  return PMPI_Finalize();
}
